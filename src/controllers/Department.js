import {computed, observable} from "mobx";
import Province from "./Province";

class Department {

    @observable _provinces = new Map();
    @observable department = null;
    parent = {id: '-', name: '-'}

    constructor(department, province, city){
        this.department = department;
        this.addProvince(province, city);
    }

    @computed get name() {
        return this.department.name;
    }

    @computed get id() {
        return this.department.id;
    }

    @computed get provinces() {
        return Array.from(this._provinces).map(([, p]) => p) || [];
    }

    @computed get subItems() {
        return this.provinces;
    }

    addProvince(province, city){
        if(province) {
            const mappedProvince = this._provinces.get(province.id);
            if (mappedProvince) {
                mappedProvince.addCity(city);
            } else {
                this._provinces.set(province.id, new Province(province, city));
            }
        }
    }
}

export default Department;