import axios from 'axios'

class ResService {

    static fetchResource() {
        const endpoint = '/resources/list.txt';
        return axios.get(endpoint)
            .then(response => ResService.processText(response.data))
            .catch(error => console.log(error))
    };

    static removeSpecialChars(textList) {
        return textList.map(line => line.replace(/(“)/g, '').replace(/(”)/g, ''));
    }

    static processText(plainText) {
        const textList = plainText.match(/(“).*?(\/).*?(\/).*?(”)/g);
        return ResService.removeSpecialChars(textList);
    }
}

export default ResService;