> React test

> Tech Stack

- CRA 3
- Craco
- React Hooks
- MobX
- Axios

> Clone and test

```shell
$ git clone https://gitlab.com/whish/test-react.git

$ cd test-react 
$ npm install && npm start 
```