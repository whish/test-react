### 0.2.0 (2019-08-08)

##### Chores

*  add generate changelog (ae5d50fe)
*  set readme (1b346856)
*  add support for decorators (f94ed90c)
*  ignore ide files (273c9083)
*  install axios and mobx dependencies (ef8c03c3)

##### New Features

* **Table:**  add LocationTable component (df07a6fb)
* **App:**  add useEffect to fetch text file (b3e765b2)
* **TextStore:**
  *  add parent to output list items (0833b0e4)
  *  add list generator methods (48355cdc)
  *  add store (8fcf9fbe)
* **Department:**
  *  add getters for name, id and subItems feat(Province): add getters for name, id and subItems (c0e94bc7)
  *  add department class (a64802c4)
* **Province:**  add Province class (5838de10)
* **resService:**  add removeSpecialChars and processText methods (ce9f199b)
* **Service:**  add fetch method (8930852e)

##### Bug Fixes

* **Province:**  remove observable decorator to prevent computed conflict (dcc58447)

##### Other Changes

*  remove console.log (95c2c092)
*  add example text file (d3957a00)

