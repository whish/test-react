import {computed, observable} from "mobx";

class Province {

    @observable _cities = new Set();
    @observable province = null;
    parent = null;

    constructor(province, city){
        this.province = province;
        this.addCity(city)
    }

    setParent(parent) {
        this.parent = parent;
    }

    @computed get name() {
        return this.province.name;
    }

    @computed get id() {
        return this.province.id;
    }

    addCity(city) {
        if (city) this._cities.add(city);
    }

    @computed get cities() {
        return Array.from(this._cities) || []
    }

    @computed get subItems() {
        return this.cities;
    }
}

export default Province;