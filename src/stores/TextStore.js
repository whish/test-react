import ResService from "../services/ResService";
import {computed, observable} from "mobx";
import Department from "../controllers/Department";


class TextStore {

    @observable _departments = new Map();

    constructor() {
        this.init()
    }

    init() {
        ResService.fetchResource()
            .then(response => {
                TextStore.processResource(response).forEach(item => {
                    this.processDepartment(item);
                });
            })
    }

    static genetateTuple(text) {
        if(text.length) {
            const id = text.match(/\d+/)[0];
            const name = text.replace(id, '').trim();
            return {id, name};
        }
    }

    processDepartment(item) {
        const department = TextStore.genetateTuple(item[0]);
        const province = TextStore.genetateTuple(item[1]);
        const city = TextStore.genetateTuple(item[2]);
        const mappedDept = this._departments.get(department.id);
        if (mappedDept) {
            mappedDept.addProvince(province, city)
        } else {
            this._departments.set(department.id, new Department(department, province, city));
        }
    }

    static processResource(resource) {
            return resource.map(res => res.split('/').map(i => i.trim()));
    }

    @computed get departments() {
        return Array.from(this._departments).map(([, d]) => d);
    }

    @computed get provinces() {
        return this.departments.reduce((result, location) => {
            return result.concat(location.subItems.map(item => {
                    item.setParent({id: location.id, name: location.name});
                    return item;
                }
            ));
        }, []);
    }

    @computed get cities() {
        return this.provinces.reduce((result, location) => {
            return result.concat(location.subItems.map(item => ({ ...item, parent: { id: location.id, name: location.name }})));
        }, []);
    }
}

export default TextStore;