import React from 'react';
import './LocationTable.css';
import {observer} from "mobx-react";


const LocationTable = observer(({ locationList }) => {
    return (
        <div className="tableWrapper">
            <table border={1}>
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Codigo padre</th>
                        <th>Descripcion padre</th>
                    </tr>
                </thead>
                <tbody>
                {locationList.map(location => (
                    <tr key={location.id}>
                        <td>{location.id}</td>
                        <td>{location.name}</td>
                        <td>{location.parent.id}</td>
                        <td>{location.parent.name}</td>
                    </tr>
                ))
                }
                </tbody>
            </table>
        </div>
    )
});

export default LocationTable