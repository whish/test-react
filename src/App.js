import React, { useEffect, useCallback, useState } from 'react';
import './App.css';
import TextStore from "./stores/TextStore";
import LocationTable from "./components/LocationTable";
import {observer} from "mobx-react";

const App = observer(() => {
  const [store, setStore] = useState(null);
  const fetchData = useCallback(() => {
    setStore(new TextStore());
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  if (!store) return <div>Loading...</div>;

  return (
    <div className="App">
      <LocationTable  locationList={store.departments} />
      <LocationTable  locationList={store.provinces} />
      <LocationTable  locationList={store.cities} />
    </div>
  );
});

export default App;
